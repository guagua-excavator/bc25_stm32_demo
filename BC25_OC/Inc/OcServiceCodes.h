/***
*OcServiceCodes.h - 定义上下行数据的结构体，还有提供组装上行报文的函数和解析下行报文的函数
*
*Purpose:
*	1.数据结构体命名和平台定义的服务标识一致
*	2.codeDataReportByIdToStr、codeDataReportByIdToBytes、codeDataReportByIdentifierToStr、codeDataReportByIdentifierToBytes为组装上报数据的函数，具体说明见函数前的注释
*	3.decodeCmdDownFromStr、decodeCmdDownFromBytes为解析平台发送过来数据的函数，具体说明见函数前的注释
****/
#ifndef OCSERVICECODES_H
#define OCSERVICECODES_H

#include <stdlib.h>
#include <string.h>


#define OC_BIG_ENDIAN 'b'
#define OC_LITTLE_ENDIAN 'l'

static union { char c[4]; unsigned long mylong; } endian_test = {{ 'l', '?', '?', 'b' } };
#define OC_ENDIANNESS ((char)endian_test.mylong)


typedef unsigned long long uint_64;
typedef unsigned int uint_32;  
typedef unsigned short uint_16;

//命令解析响应码
#define OC_CMD_SUCCESS 0						//执行成功
#define OC_CMD_FAILED 1						//执行失败
#define OC_CMD_INVALID_DATASET_TYPE 2			//无效数据集类型
#define OC_CMD_INVALID_DATASET_IDENTIFIER 3	//无效数据集标识
#define OC_CMD_PAYLOAD_PARSING_FAILED 4		//指令数据集Payload解析失败,紧凑二进制编码内容长度不符等


typedef struct OcStrStruct
{
	unsigned short len;
	char* str;
} OcString;
typedef OcString OcBytes;

//无符号整型16位  
uint_16 oc_htons(uint_16 source);

//无符号整型32位
uint_32 oc_htoni(uint_32 source);

//无符号整型64位
uint_64 oc_htonl(uint_64 source);

//float
float oc_htonf(float source);

//double
double oc_htond(double source);

//16进制转字符串
void HexToStr(char *pbDest, char *pbSrc, int nLen);

//字符串转16进制
void StrToHex(char *pbDest, char *pbSrc, int nLen);


//根据服务id生成上报数据的十六进制字符串,srcStruct需要根据服务定义传入对应的类型,返回结果为字符串
OcString codeDataReportByIdToStr(int serviceId, void * srcStruct);

//根据服务id生成上报数据的字节流,srcStruct需要根据服务定义传入对应的类型,返回结果为字节流
OcBytes codeDataReportByIdToBytes(int serviceId, void * srcStruct);

//根据服务标识生成上报数据的十六进制字符串,srcStruct需要根据服务定义传入对应的类型,返回结果为字符串
OcString codeDataReportByIdentifierToStr(char * serviceIdentifier, void * srcStruct);

//根据服务标识生成上报数据的字节流,srcStruct需要根据服务定义传入对应的类型,返回结果为字节流
OcBytes codeDataReportByIdentifierToBytes(char * serviceIdentifier, void * srcStruct);

//指令解析返回结构体，data在使用时需要根据serviceId强转为对应类型
typedef struct CmdStruct 
{
	char* serviceIdentifier;
	unsigned short taskId;
	void * data;
	int code;
} OcCmdData;
//解析接受到的报文数据,入参为十六进制字符串
OcCmdData decodeCmdDownFromStr(char* source);
//解析接受到的报文数据,入参为原始字节流
OcCmdData decodeCmdDownFromBytes(char* source, int len);

void str2hex(char* str, char* hex);

typedef struct data_reportStruct 
{
	int temperature;		// 设定温度
	char fan_left_right;	// 左右摆风
	char fan_up_down;		// 上下摆风
	char onoff_state;		// 开关机状态
	char mode;				// 运行模式
	char current_speed;		// 当前风速
} data_report;
//数据上报:业务数据上报
OcString data_report_CodeDataReport (data_report srcStruct);


typedef struct error_code_reportStruct 
{
	char error_code;
} error_code_report;
//事件上报:故障上报
OcString error_code_report_CodeEventReport (error_code_report srcStruct);


typedef struct set_runStruct 
{
	char onoff_state;
	char mode;
	char current_speed;
	char fan_up_down;
	char fan_left_right;
	int temperature;
} set_run;
//指令下发:设定运行状态
int set_run_DecodeCmdDown (char* source, set_run* dest);


typedef struct set_run_respStruct 
{
	unsigned short taskId;
	int result;
} set_run_resp;
//指令下发响应:设定运行状态响应
OcString set_run_resp_CodeCmdResponse (set_run_resp srcStruct);


typedef struct signal_reportStruct 
{
	int rsrp;		// 参考信号接收功率
	int sinr;		// 信号与干扰加噪声比
	int pci;		// 物理小区标识
	int ecl;		// 无线信号覆盖等级
	int cell_id;	// 小区位置信息
	
} signal_report;	
//数据上报:信号数据上报
OcString signal_report_CodeDataReport (signal_report srcStruct);

#endif
