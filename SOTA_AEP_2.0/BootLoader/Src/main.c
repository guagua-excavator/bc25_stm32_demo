/* USER CODE BEGIN Header */
/****************************************************************************
*
* Copy right: 2020-, Copyrigths of Quectel Ltd.
****************************************************************************
* File name: main.c
* History: Rev1.0 2020-12-1
****************************************************************************/
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "dbg_log.h"
#include "flash.h"

#include "stdlib.h"
#include "string.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */


/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

//|   区域名称    | flash占用大小  | flash开始地址 | flash结束地址 |       备注       |
//| :-----------: | :------------: | :-----------: | :-----------: | :--------------: |
//| STM32F103ZET6 | 512KB(0x80000) |   0x8000000   |   0x8080000   | stm32_FLASH空间  |
//|  BootLoader   |  16kb(0x4000)  |   0x8000000   |   0x8004000   |   软件引导程序   |
//|     APP1      | 64kb(0x10000)  |   0x8004000   |   0x8014000   |   用于存放APP1   |
//|     APP2      | 64kb(0x10000)  |   0x8014000   |   0x8024000   |   用于存放APP2   |
//|   NV 数据区   |   2kb(0x800)   |   0x8024000   |   0x8024800   | 用于存放关键数据 |

//#define FLASH_BASE 			0x8000000
#define BootLoader_ADDR 		FLASH_BASE
#define BootLoader_SIZE 		0x4000

#define FLASH_APP_ADDR 				(FLASH_BASE+BootLoader_SIZE)
#define FLASH_APP_SIZE 				0x10000

#define FLASH_APP_TEMP_ADDR 		(FLASH_APP_ADDR+FLASH_APP_SIZE)
#define FLASH_APP_TEMP_SIZE 		0x10000

#define FLASH_NV_ADDR 				(FLASH_APP_TEMP_ADDR+FLASH_APP_TEMP_SIZE)
#define FLASH_NV_SIZE 				0x800

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

typedef struct{
	
	uint8_t new_version[16];		// new版本号
	uint16_t single_package;		// 单包字节数
	uint16_t total_package;			// 总包数
	uint16_t ver_pack_code;			// 校验码		

} sota_new_ver_t;

typedef struct{

	uint8_t sota_status;			// 状态
	uint16_t download_pack;		  	// 已下载包数
	uint8_t fail_times;				// 失败次数 	bootloader中检查，是否是下载模式?
	uint8_t old_version[16];		// old版本号
	sota_new_ver_t sota_new_ver;
	
} sota_type_t;

typedef enum {
	
	RUN_MODE = 0x01,
	DOWNLOAD_MODE,
	UPGRADE_MODE
	
} mcu_run_status_t;

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */
sota_type_t flash_data_init(void);
void jump_app(uint32_t app_addr);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
uint16_t tmp_data[1024]={0};
uint32_t tmp_addr = 0x00;
uint32_t hand_addr = 0x00;

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART1_UART_Init();
  /* USER CODE BEGIN 2 */
		
	LOG_D(" bootLoader runing...");

	sota_type_t sota_type = flash_data_init();

	if(sota_type.sota_status == RUN_MODE)
	{
		jump_app(FLASH_APP_ADDR);
	}
	else if(sota_type.sota_status == UPGRADE_MODE)
	{
		LOG_D("Data area handling...");
		// 搬运数据到app区  
		// 1、sota_type.sota_new_ver.ver_pack_code 校验所有搬运出来的数据
		tmp_addr = FLASH_APP_TEMP_ADDR;
		while((FLASH_APP_TEMP_ADDR+FLASH_APP_TEMP_SIZE) >= tmp_addr)
		{	
			flash_read(FLASH_APP_TEMP_ADDR+hand_addr,tmp_data,1024);
			flash_write(FLASH_APP_ADDR+hand_addr,tmp_data,1024);
			tmp_addr += (1024*2);
			hand_addr += (1024*2);
		}

		sota_type.sota_status = RUN_MODE;
		sota_type.download_pack = 0x0000;
		sota_type.fail_times = 0x00;
		memset(sota_type.old_version,0,16);
		memcpy(sota_type.old_version,sota_type.sota_new_ver.new_version,strlen((char* )sota_type.sota_new_ver.new_version));
		memset(&sota_type.sota_new_ver,0,sizeof(sota_new_ver_t));
		
		HAL_FLASH_Unlock();
		FLASH_PageErase(FLASH_NV_ADDR);
		FLASH_WaitForLastOperation(FLASH_WAITETIME);
		CLEAR_BIT(FLASH->CR, FLASH_CR_PER);
		
		for(uint8_t i = 0;i < sizeof(sota_type_t);i+=8)
		{
			if(HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, FLASH_NV_ADDR+i,*((__IO uint64_t*)(&sota_type.sota_status+i)))!= HAL_OK)//这里的数据由于是结构体,需要从里面取值,一定要以第一个成员的地址开始偏移,不能使用结构体自身的地址来偏移,否则数据会出错
			{
				HAL_FLASH_Lock();
			}
		}
		HAL_FLASH_Lock();		
		
		// 软件重启
		__set_FAULTMASK(1); 
		NVIC_SystemReset();
	}
	else if(sota_type.sota_status == DOWNLOAD_MODE)
	{
		// 直接计数错误
		sota_type.fail_times++;

		HAL_UART_MspDeInit(&huart1);		
		jump_app(FLASH_APP_ADDR);	
	}
	else
	{
		LOG_D(" BootLoader failure!");
	}

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
    while (1)
    {
		HAL_Delay(1000);
		
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
    }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

#define CURRENT_VERSION        "V1.0"
sota_type_t flash_data_init(void)
{
	sota_type_t sota_type;
	memset(&sota_type,0,sizeof(sota_type_t));
	
	if(flash_read_halfword(FLASH_NV_ADDR) == 0xffff)
	{
		sota_type.sota_status = RUN_MODE;
		sota_type.download_pack = 0x0000;
		sota_type.fail_times = 0x00;
		memcpy(sota_type.old_version,CURRENT_VERSION,strlen(CURRENT_VERSION));
		
		HAL_FLASH_Unlock();
		FLASH_PageErase(FLASH_NV_ADDR);
		FLASH_WaitForLastOperation(FLASH_WAITETIME);
		CLEAR_BIT(FLASH->CR, FLASH_CR_PER);
		
		for(uint8_t i = 0;i < sizeof(sota_type_t);i+=8)
		{
			if(HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, FLASH_NV_ADDR+i,*((__IO uint64_t*)(&sota_type.sota_status+i)))!= HAL_OK)//这里的数据由于是结构体,需要从里面取值,一定要以第一个成员的地址开始偏移,不能使用结构体自身的地址来偏移,否则数据会出错
			{
				HAL_FLASH_Lock();
			}
		}
		HAL_FLASH_Lock();
	}
	for(uint8_t i = 0;i<sizeof(sota_type_t);i+=8)
	{
		*((uint64_t *)(&sota_type.sota_status+i)) = *(__IO uint64_t*)(FLASH_NV_ADDR + i);//注意赋值的左边,必须要用结构体第一个成员的地址来偏移,双字偏移量是8
	}
	
	LOG_D("sota_type.sota_status:      < %02X >", sota_type.sota_status);
	LOG_D("sota_type.download_pack:    < %04X >", sota_type.download_pack);
	LOG_D("sota_type.fail_times:       < %02X >", sota_type.fail_times);
	LOG_D("sota_type.old_version:      < %s >", sota_type.old_version);
	LOG_D("sota_type.new_version:      < %s >", sota_type.sota_new_ver.new_version);
	LOG_D("sota_type.single_package:   < %04X >", sota_type.sota_new_ver.single_package);
	LOG_D("sota_type.total_package:    < %04X >", sota_type.sota_new_ver.total_package);
	LOG_D("sota_type.ver_pack_code:    < %04X >", sota_type.sota_new_ver.ver_pack_code);	
	
	return sota_type;
}

void jump_app(uint32_t app_addr)
{
	if(((*(volatile uint32_t*)(app_addr+4))&0xFF000000)==0x08000000)
	{	
		HAL_UART_DeInit(&huart1);
		iap_load_app(app_addr);//执行FLASH APP代码
	}
	else 
	{
		LOG_D(" Execution <%X> failure!",app_addr);
	}	
}


/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
    /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
    /* User can add his own implementation to report the file name and line number,
       tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
