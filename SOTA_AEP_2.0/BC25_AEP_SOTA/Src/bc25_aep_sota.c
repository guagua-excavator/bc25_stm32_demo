/****************************************************************************
*
* Copy right: 2020-, Copyrigths of Quectel Ltd.
****************************************************************************
* File name: bc25_aep_sota.c
* History: Rev1.0 2020-12-1
****************************************************************************/

#include "bc25_aep_sota.h"

#include "dbg_log.h"
#include "middleware.h"
#include "at_cmd.h"
#include "at.h"
#include "flash.h"
#include "bc25_aep.h"


#include <string.h>
#include <stdio.h>

#define STANDARD_PCP_LEN	8
#define SOFTWARE_VER_LEN	16

sota_type_t sota_type;

/**
 * @description: 上报软件版本号
 * @param version_str 当前软件版本号
 * @return 	Q_EOK 	成功
 * 			OTHER	失败
 */
int8_t upload_software_version(char* version_str)
{
	int8_t result = Q_EOK;
	int i = 0;
	int zeroLen = 0;
	int hexLen = 0;
	char tmep_ver[100];
	char tmep_hex[100];
	uint8_t tmep_crc[100];
	char send_cmd[100];

	const char* cHex = "0123456789ABCDEF";
	
	/* 这里可以直接从FLASH将版本数据获取到，然后做好处理直接输出*/  
	if(strlen(version_str) > SOFTWARE_VER_LEN)
	{
		LOG_D("不能大于%d\r\n",strlen(version_str));
		return Q_EFULL;
	}
	
	zeroLen = SOFTWARE_VER_LEN - strlen(version_str);
	memset(tmep_ver,0,SOFTWARE_VER_LEN);

	for(int j =0; j < strlen(version_str); j++)
	{
		unsigned int a =  (unsigned int) version_str[j];
		tmep_ver[i++] = cHex[(a & 0xf0) >> 4];
		tmep_ver[i++] = cHex[(a & 0x0f)];
	}

	for(i = 0; i < zeroLen; i++)
	{
		strcat(tmep_ver,"00");
	}

	sprintf(tmep_hex,"%04X%02X%02X%04X%04X%02X%s",PCP_HEAD_TYPEY,PCP_SOTA_VERSION,QUERY_VERSION,0,17,0,tmep_ver);
	
	LOG_D("Test:%s\r\n",tmep_hex);
	LOG_D("TestLen:%d\r\n",strlen(tmep_hex));
	
	memset(tmep_crc,0,100);	
	hexLen = hex_str_to_byte(tmep_hex,tmep_crc);
	
	int crc_value = do_crc((uint8_t*)tmep_crc,hexLen);
	
	LOG_D("crc_value ==> %x\n", crc_value);
	
	memset(send_cmd,0,100);	
	sprintf(send_cmd,"AT+NMGS=%d,%04X%02X%02X%04X%04X%02X%s",hexLen,PCP_HEAD_TYPEY,PCP_SOTA_VERSION,QUERY_VERSION,crc_value,17,0,tmep_ver);

	result = at_send_cmd(send_cmd, AT_OK, 0, 3000);
	if(result != Q_EOK)
	{
		LOG_D("check_send_cmd: %s is ERROR,result: %d !",send_cmd, result);
	}
	return result;		
}

/**
 * @description: 平台新版本通知答复
 * @param data 待答复信息
 * @return 	Q_EOK 	成功
 * 			OTHER	失败
 */
int8_t new_version_inform(char* data)
{
	int8_t result = Q_EOK;
	char tmep_hex[100];
	uint8_t tmep_crc[100];
	char send_cmd[100];
	
	sota_new_ver_t* new_version = (sota_new_ver_t*)data;
	
	new_version->total_package = aep_htons(new_version->total_package);
	new_version->single_package = aep_htons(new_version->single_package);
	new_version->ver_pack_code = aep_htons(new_version->ver_pack_code);
	
	// 保存升级信息
	memcpy(&sota_type.sota_new_ver,new_version,sizeof(sota_new_ver_t));
	
	memset(tmep_crc,0,100);	
	sprintf(tmep_hex,"%04X%02X%02X%04X%04X%02X",PCP_HEAD_TYPEY,PCP_SOTA_VERSION,NEW_VERSION_INFORM,0,1,ALLOW_UPGRADE);
	
	int hexLen = hex_str_to_byte(tmep_hex,tmep_crc);			

	int crc_value = do_crc((uint8_t*)tmep_crc,hexLen);
	LOG_D("crc_value ==> %x\n", crc_value);
	
	memset(send_cmd,0,100);	
	sprintf(send_cmd,"AT+NMGS=%d,%04X%02X%02X%04X%04X%02X",hexLen,PCP_HEAD_TYPEY,PCP_SOTA_VERSION,NEW_VERSION_INFORM,crc_value,1,ALLOW_UPGRADE);
	
	result = at_send_cmd(send_cmd, AT_OK, 0, 3000);
	if(result != Q_EOK)
	{
		LOG_D("check_send_cmd: %s is ERROR,result: %d !",send_cmd, result);
	}
	return result;
}

/**
 * @description: 请求平台下载数据包
 * @param None
 * @return 	Q_EOK 	成功
 * 			OTHER	失败
 */
int8_t download_data_packet(void)
{
	// AT+NMGS=26,FFFE 01 15 72A5 0012 56322E36000000000000000000000000 0000
	int8_t result = Q_EOK;
	int i = 0;
	int zeroLen = 0;
	int hexLen = 0;
	char tmep_ver[100];
	char tmep_hex[100];
	uint8_t tmep_crc[100];
	char send_cmd[100];
	
	char* ver_str = (char* )sota_type.sota_new_ver.new_version;
	
	LOG_D("versions:%s\r\n",ver_str);
	
	if(strlen(ver_str) > SOFTWARE_VER_LEN)
	{
		LOG_D("不能大于%d\r\n",strlen(ver_str));
		return Q_EFULL;
	}
	
	zeroLen = SOFTWARE_VER_LEN - strlen(ver_str);
	memset(tmep_ver,0,SOFTWARE_VER_LEN);

	const char* cHex = "0123456789ABCDEF";
    
	for(int j =0; j < strlen(ver_str); j++)
	{
		unsigned int a =  (unsigned int) ver_str[j];
		tmep_ver[i++] = cHex[(a & 0xf0) >> 4];
		tmep_ver[i++] = cHex[(a & 0x0f)];
	}

	for(i = 0; i < zeroLen; i++)
	{
		strcat(tmep_ver,"00");
	}
	
	memset(tmep_hex,0,100);

	sprintf(tmep_hex,"%04X%02X%02X%04X%04X%s%04X",PCP_HEAD_TYPEY,PCP_SOTA_VERSION,REQUEST_UPGRADE_PACKAGE,0,18,tmep_ver,sota_type.download_pack);
	
	LOG_D("Test:%s\r\n",tmep_hex);
	LOG_D("TestLen:%d\r\n",strlen(tmep_hex));
	
	memset(tmep_crc,0,100);	
	hexLen = hex_str_to_byte(tmep_hex,tmep_crc);
	
	int crc_value = do_crc((uint8_t*)tmep_crc,hexLen);
	
	LOG_D("crc_value ==> %x\n", crc_value);
	
	memset(send_cmd,0,100);	
	
	sprintf(send_cmd,"AT+NMGS=%d,%04X%02X%02X%04X%04X%s%04X,100",hexLen,PCP_HEAD_TYPEY,PCP_SOTA_VERSION,REQUEST_UPGRADE_PACKAGE,crc_value,18,tmep_ver,sota_type.download_pack);
	
	result = at_send_cmd(send_cmd, AT_OK, 0, 3000);
	if(result != Q_EOK)
	{
		LOG_D("check_send_cmd: %s is ERROR,result: %d !",send_cmd, result);
	}
	return result;
}

/**
 * @description: 上报升级包下载状态 (校验全包)
 * @param None
 * @return 	Q_EOK 	成功
 * 			OTHER	失败
 */
int8_t resp_upgrade_pack(void)
{
	int8_t result = Q_EOK;
	char send_cmd[100] = "AT+NMGS=9,FFFE0116850E000100";
	if(sota_type.download_pack == sota_type.sota_new_ver.total_package)
	{
		LOG_D("OK OK OK OK OK OK OK OK\r\n");
		result = at_send_cmd(send_cmd, AT_OK, 0, 3000);
		if(result != Q_EOK)
		{
			LOG_D("check_send_cmd: %s is ERROR,result: %d !",send_cmd, result);
		}
		return result;
	}
	else
	{

		set_except_event(SOTA_DOWNLOAD_RESPONSE_SUCC);
	}
	return result;
}

/**
 * @description: 执行升级 
 * @param None
 * @return 	Q_EOK 	成功
 * 			OTHER	失败
 */
int8_t excute_upgrade(void)
{
	int8_t result = Q_EOK;
	char send_cmd[100] = "AT+NMGS=9,FFFE0117B725000100";

	result = at_send_cmd(send_cmd, AT_OK, 0, 3000);
	if(result != Q_EOK)
	{
		LOG_D("check_send_cmd: %s is ERROR,result: %d !",send_cmd, result);
	}
	return result;
}

/**
 * @description: 上报升级结果 
 * @param None
 * @return 	Q_EOK 	成功
 * 			OTHER	失败
 */
int8_t re_upgrade_result(void)
{
	int8_t result = Q_EOK;
	char send_cmd[100] = "AT+NMGS=25,FFFE0118AD2600110056322E36300000000000000000000000";

	result = at_send_cmd(send_cmd, AT_OK, 0, 3000);
	if(result != Q_EOK)
	{
		LOG_D("check_send_cmd: %s is ERROR,result: %d !",send_cmd, result);
	}
	return result;
}


uint16_t read_data[250];
/**
 * @description: 数据检查 最后一包数据 可能存在差异 无影响 
 * @param pack_data 包数据
 * @return 	Q_EOK 	成功
 * 			OTHER	失败
 */
void bc25_aep_sota_data(char* pack_data)
{
	uint16_t i = 0;

	int8_t result = Q_EOK;
	uint16_t tmp_short = 0;
	uint16_t str_to_byte_len = 0;
	
	pcp_type_t pcp_type;
	char* data_index = pack_data;
	uint8_t* tmp_data;
	uint16_t dis_data_packs;
	
	memset(&pcp_type, 0, sizeof(pcp_type_t));

	// 数据转换
	str_to_byte_len = hex_str_to_byte((char* )data_index, (uint8_t* )(&pcp_type));
	tmp_short = aep_htons(pcp_type.check_code);
	pcp_type.check_code = 0;
	
	tmp_data = (uint8_t* )(&pcp_type);
	LOG_D("tmp_short: <0x%X> do_crc: <0x%X>\r\n", tmp_short,do_crc(tmp_data, str_to_byte_len));
	
	if(tmp_short == do_crc(tmp_data, str_to_byte_len))
	{
		pcp_type.frame_header = aep_htons(pcp_type.frame_header);
		pcp_type.check_code = tmp_short;
		pcp_type.length = aep_htons(pcp_type.length);

		LOG_D("pcp_type.frame_header:    <0x%X>\r\n", pcp_type.frame_header);
		LOG_D("pcp_type.version:         <0x%X>\r\n", pcp_type.version);
		LOG_D("pcp_type.message_code:    <0x%X>\r\n", pcp_type.message_code);
		LOG_D("pcp_type.check_code:      <0x%X>\r\n", pcp_type.check_code);
		LOG_D("pcp_type.length:          <0x%X>\r\n", pcp_type.length);
	
		switch(pcp_type.message_code)
		{
			case QUERY_VERSION:
				
				LOG_D("QueryVersion:<%d>\r\n",upload_software_version("V1.0"));
			
				break;
			case NEW_VERSION_INFORM:
				
				result = new_version_inform((char* )pcp_type.data);
				LOG_D("NewVersionInform:<%d>\r\n",result);
				if(result == Q_EOK)
				{	
					// 切换为下载模式
					sota_type.sota_status = DOWNLOAD_MODE;
					// 允许下载数据
					set_except_event(SOTA_DOWNLOAD_RESPONSE_SUCC);
				}
				break;
			case REQUEST_UPGRADE_PACKAGE:
				
				iap_write_appbin((FLASH_APP_TEMP_ADDR+(sota_type.sota_new_ver.single_package*sota_type.download_pack)),pcp_type.data+3,sota_type.sota_new_ver.single_package);
		
				printf("\r\n\r\n*******************data comparison*****************\r\n\r\n");
			
				memset(read_data,0,250*sizeof(uint16_t));
			
				flash_read((FLASH_APP_TEMP_ADDR+(sota_type.sota_new_ver.single_package*sota_type.download_pack)),read_data,250);

				for(i = 0; i < 500; i+=2)
				{
					if((pcp_type.data[i+3] | pcp_type.data[i+4] << 8) != read_data[(i/2)])
					{
						printf("data:[<%d>]\r\n",i);
						printf("data:[<%x>][<%x>]\r\n",(pcp_type.data[i+3] | pcp_type.data[i+4] << 8),read_data[i]);
					}
				}
				printf("\r\n\r\n*****************data comparison*******************\r\n\r\n");
				
				dis_data_packs = (uint16_t)(pcp_type.data[1] << 8 | pcp_type.data[2]); 
				LOG_D("\r\ndata_pack: <%04X> <%04X>\r\n",dis_data_packs,sota_type.download_pack);
				if(sota_type.download_pack == dis_data_packs)
				{
					// 增加下载包数量
					sota_type.download_pack++;
				}
				
				HAL_FLASH_Unlock();
				FLASH_PageErase(FLASH_NV_ADDR);
				FLASH_WaitForLastOperation(FLASH_WAITETIME);
				CLEAR_BIT(FLASH->CR, FLASH_CR_PER);
				
				for(uint8_t i = 0;i < sizeof(sota_type_t);i+=8)
				{
					if(HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, FLASH_NV_ADDR+i,*((__IO uint64_t*)(&sota_type.sota_status+i)))!= HAL_OK)//这里的数据由于是结构体,需要从里面取值,一定要以第一个成员的地址开始偏移,不能使用结构体自身的地址来偏移,否则数据会出错
					{
						HAL_FLASH_Lock();
					}
				}
				HAL_FLASH_Lock();
				
				// 接收平台下发数据包，并判断是否接收完毕，接收完毕。上报升级包下载状态
				resp_upgrade_pack();

				LOG_D("RequestUpGradePackage\r\n");

				break;
			case RE_UPGRADE_PACK_DOWNLOAD_STA:
				
				LOG_D("RE_UPGRADE_PACK_DOWNLOAD_STA\r\n");
				
				break;
			case EXCUTE_UPGRADE:
				// 平台主动下发，开始执行升级动作
				LOG_D("EXCUTE_UPGRADE\r\n");
				
				excute_upgrade();
			
				/* 开始执行升级，升级完毕后，执行上报*/
				
				re_upgrade_result();
			
				break;
			case RE_UPGRADE_RESULT:
				
				re_upgrade_result();
				
				sota_type.sota_status = UPGRADE_MODE;
			
				HAL_FLASH_Unlock();
				FLASH_PageErase(FLASH_NV_ADDR);
				FLASH_WaitForLastOperation(FLASH_WAITETIME);
				CLEAR_BIT(FLASH->CR, FLASH_CR_PER);
				for(uint8_t i = 0;i < sizeof(sota_type_t);i+=8)
				{
					if(HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, FLASH_NV_ADDR+i,*((__IO uint64_t*)(&sota_type.sota_status+i)))!= HAL_OK)//这里的数据由于是结构体,需要从里面取值,一定要以第一个成员的地址开始偏移,不能使用结构体自身的地址来偏移,否则数据会出错
					{
						HAL_FLASH_Lock();
					}
				}
				HAL_FLASH_Lock();
			
				LOG_D(" RE_UPGRADE_RESULT \r\n");
				
				__set_FAULTMASK(1); 
				NVIC_SystemReset();
				
				break;
			default:
				break;
		}
	}
}
