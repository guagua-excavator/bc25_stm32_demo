/****************************************************************************
*
* Copy right: 2020-, Copyrigths of Quectel Ltd.
****************************************************************************
* File name: bc25_aep.c
* History: Rev1.0 2020-12-1
****************************************************************************/

#include "bc25_aep.h"

#include "at_cmd.h"
#include "dbg_log.h"
#include "middleware.h"
#include "AepServiceCodes.h"
#include "bc25_aep_sota.h"

#include "gpio.h"

#include <string.h>
#include <stdio.h>

#define AT_NCFG_LIFETINE    	"AT+NCFG=%d,%d"
#define AT_NCDPOPEN    			"AT+NCDPOPEN=\"%s\",%d"
#define AT_NCDPCLOSE    		"AT+NCDPCLOSE"
#define AT_NNMI   				"AT+NNMI=%d"
#define AT_NMGS_NON				"AT+NMGS=%d,%s"
#define AT_NMGS_CON   			"AT+NMGS=%d,%s,%d"


event_except_info_t g_except_info = EVENT_END;

aep_param_t g_aep_param = {	
	
	"221.229.214.202",						// IP 地址
	5683,									// 端口
};

static void qlwevtind_urc_handler(const char *data, uint16_t size);
static void nnmi_urc_handler(const char *data, uint16_t size);
static void deepsleep_urc_handler(const char *data, uint16_t size);
static void wakeup_urc_handler(const char *data, uint16_t size);

/**
 * @description: URC 匹配表
 */
static const struct at_urc urc_table[] = {
    { "+QLWEVTIND:", "\r\n", qlwevtind_urc_handler},
	{ "+NNMI:", "\r\n", nnmi_urc_handler},
	{ "+QATSLEEP:", "\r\n", deepsleep_urc_handler},
	{ "+QATWAKEUP:", "\r\n", wakeup_urc_handler},
};

/**
 * @description: +QATSLEEP URC处理函数
 */
static void deepsleep_urc_handler(const char *data, uint16_t size)
{
	LOG_D("%s\r\n",data);
}

/**
 * @description: +QATWAKEUP URC处理函数
 */
static void wakeup_urc_handler(const char *data, uint16_t size)
{
	LOG_D("%s\r\n",data);
}

// 接收数据缓存
char nnmi_data[2048];
uint16_t nnmi_data_len = 0;

/**
 * @description: +NNMI URC处理函数
 */
static void nnmi_urc_handler(const char *data, uint16_t size)	
{
	AepCmdData result;
	
	const char* data_index = data;
	char nnmi_str_len[4];
	uint16_t data_str_len = 0;

	// LOG_I("data:%s\r\n",data_index);
	
	*((char* )(data_index+strlen(data_index)-2)) = '\0';
	
	//+NNMI: 16,06 1F41 0025 0009 010000000000000015
	// 提取数据长度成功
	if(!get_str((char* )(data_index+strlen("+NNMI: ")),nnmi_str_len,0))
	{
		LOG_I("nnmi data extraction failed!\r\n");
		return;
	}
	data_str_len = atoi(nnmi_str_len);
	
	// 提取数据
	memset(nnmi_data,0,sizeof(char)*1024);
	get_str((char* )data_index,nnmi_data,1);
//	LOG_I("nnmi_data:<%s><%d>\r\n",nnmi_data,strlen(nnmi_data));
	
	nnmi_data_len = strlen(nnmi_data)/2;
	
	// 数据有效性初步检查
	if(nnmi_data_len != data_str_len)
	{
		LOG_I("nnmi data verification failed,nnmi_data_len:<%d>,data_str_len:<%d>\r\n",nnmi_data_len,data_str_len);
		return;
	}
	LOG_I("nnmi data verification succeeded!\r\n");
	
	// FFFE01134C9A0000
	if(nnmi_data[0] == 'F' && nnmi_data[1] == 'F' && nnmi_data[2] == 'F' && nnmi_data[3] == 'E')
	{
		LOG_I("****SOTA*******SOTA********SOTA*********SOTA********");
		bc25_aep_sota_data(nnmi_data);
		LOG_I("****SOTA*******SOTA********SOTA*********SOTA********");
	}
	else if(nnmi_data[0] == '0' && nnmi_data[1] == '6')
	{
		result = decodeCmdDownFromStr(nnmi_data);
		
		set_run* dest = (set_run* )result.data; 
	
		LOG_I("*******************************************************");
		LOG_I("onoff_state: %d",dest->onoff_state);
		LOG_I("mode: %d",dest->mode);
		LOG_I("current_speed: %d",dest->current_speed);
		LOG_I("fan_up_down: %d",dest->fan_up_down);
		LOG_I("fan_left_right: %d",dest->fan_left_right);
		LOG_I("temperature: %d",dest->temperature);
		LOG_I("*******************************************************");

		free(result.data);
	}
	else
	{
		;
	}
}


/**
 * @description: +QLWEVTIND URC处理函数
 */
static void qlwevtind_urc_handler(const char *data, uint16_t size)	
{
	int value;
	
	// +QLWEVTIND: 0
	sscanf(data,"%*[^ ] %d",&value);
	
	LOG_D("<-- +QLWEVTIND value :<%d> -->\r\n",value);
	switch(value)
	{
		case ERROR_IND:
		case CONNECT_SUC:
		case CONNECT_FAIL:
		case CONNECTED_OBSERD:

			if (CONNECT_SUC == value)	
			{
				// AEP Connection success
				set_except_event(NCDPOPEN_CONNECT_EVENT_SUCC);
				break;
			}
			else if(CONNECT_FAIL == value || ERROR_IND == value)
			{
				// AEP connect fail
				set_except_event(NCDPOPEN_CONNECT_EVENT_FAIL);
			}
			else if(CONNECTED_OBSERD == value)	
			{
				// AEP subscribe success
				set_except_event(NCDPOPEN_OBSERD_EVENT_SUCC);
			}
			else
			{
				LOG_I("<-- aep other:<%d> -->\r\n",value);
			}
			break;
		case MSG_SEND_SUC:
		case MSG_SEND_FAIL:
		{
			if(MSG_SEND_SUC == value)	
			{	
				// AEP SEND success
				set_except_event(NMGS_SEND_DATA_EVENT_SUCC);
				break;
			}
			else if(MSG_SEND_FAIL == value)	
			{
				// AEP SEND fail
				set_except_event(NMGS_SEND_DATA_EVENT_FAIL);
				break;
			}
			break;
		}
		case RECOV_SUC_FROM_SLEEP:
		case RECOV_FAIL_FROM_SLEEP:
		{	
			if(RECOV_SUC_FROM_SLEEP == value)	
			{
				// 睡眠唤醒后,重新建立连接成功
				// LOG_I("<-- AEP SLEEP success:<%d> -->\r\n",value);
				// set_except_event(DEEPSLEEP_WAKEUP_EVENT_SUCC);
				// break;
			}
			else if(RECOV_FAIL_FROM_SLEEP == value)	
			{
				// 睡眠唤醒后,重新建立连接失败
				// LOG_I("<-- AEP SLEEP fail:<%d> -->\r\n",value);
				// set_except_event(DEEPSLEEP_WAKEUP_EVENT_ERR);
				// break;
			}
			break;
		}
		case OBSV_FOTA_OBJ:
		case DISOBSV_FOTA_OBJ:
		{
			break;
		}
		case RECV_RST_MSG:
		{
			// 发送数据失败
			set_except_event(NMGS_SERVER_REST_EVENT_ERR);
			break;
		}
		default:
			break;
		}
}

/**
 * @description: 事件处理函数
 */
void set_except_event(event_except_info_t except_info)
{
	g_except_info = except_info;
}

/**
 * @description: 初始化函数
 * @param None
 * @return None 
 */
void bc25_init(void)
{
	at_init();;
	at_set_urc_table(urc_table, sizeof(urc_table) / sizeof(urc_table[0]));
}

/**
 * @description:  硬件复位模组
 * @param None
 * @return None
 */
void bc25_reset(void)
{	
	HAL_GPIO_WritePin(bc25_reset_pin_GPIO_Port, bc25_reset_pin_Pin, GPIO_PIN_SET);
	HAL_Delay(1000);	// 复位时间
	HAL_GPIO_WritePin(bc25_reset_pin_GPIO_Port, bc25_reset_pin_Pin, GPIO_PIN_RESET);
	HAL_Delay(3000);	// 等待模组复位	
}

/**
 * @description: Vbat电源控制
 * @param power_mode		POWER_ENABLE_MODE 		使能上电
 *							POWER_DISABLE_MODE		失能去电
 * @return None
 */
void bc25_power(enum_power_mode_t power_mode)
{	

	if(power_mode == POWER_DISABLE_MODE)
	{
		at_set_cfun(CFUN_DISABLE);
		HAL_Delay(1000);	
		HAL_GPIO_WritePin(bc25_powd_pin_GPIO_Port, bc25_powd_pin_Pin, GPIO_PIN_SET);
		HAL_Delay(1000);
	}

	if(power_mode == POWER_ENABLE_MODE)
	{
		HAL_GPIO_WritePin(bc25_powd_pin_GPIO_Port, bc25_powd_pin_Pin, GPIO_PIN_RESET);
		HAL_Delay(1000);

		HAL_GPIO_WritePin(bc25_powkey_pin_GPIO_Port, bc25_powkey_pin_Pin, GPIO_PIN_SET);
		HAL_Delay(1000);
		HAL_GPIO_WritePin(bc25_powkey_pin_GPIO_Port, bc25_powkey_pin_Pin, GPIO_PIN_RESET);
		HAL_Delay(1000);
	}
}

/**
 * @description: 模组连接,用于判断是否与模组建立连接
 * @param None
 * @return 	Q_EOK 	成功
 * 			OTHER	失败
 */
int8_t bc25_connect(void)
{	
	int8_t result = Q_EOK;
	
	int8_t count = 10;
	
	while(count > 0 && Q_EOK != at_send_cmd(AT_TEST, AT_OK, 0, 3000))
	{
		HAL_Delay(1000);
		count--;
	}
	if(count == 0)
	{
		LOG_D("<-- connection bc25 module failed! -->");
		result = Q_ERROR;
	}
	return result;
}

/**
 * @description: 
 * @param 	ncfg_mode		NCFG_LIFETIME_MODE 		配置设备在平台端的生存周期 Lifetime
 * 								NCFG_DEEPSLEEP_WAKE		配置深度睡眠唤醒后连接恢复功能
 * @param	life_time		Lifetime 				周期时间
 * @return 	Q_EOK 	成功
 * 			OTHER	失败
 */
int8_t bc25_aep_ncfg(enum_ncfg_mode_t ncfg_mode,uint32_t life_time)
{
	int8_t result = Q_EOK;
	char send_cmd[50];

	sprintf(send_cmd,AT_NCFG_LIFETINE,ncfg_mode,life_time);
	
	result = at_send_cmd(send_cmd, AT_OK, 0, 3000);
	if(result != Q_EOK)
	{
		LOG_D("check_send_cmd: %s is error,result: %d !",send_cmd, result);
	}
	return result;	
}

/**
 * @description: 配置数据接收模式
 * @param 	nnmi_status	 		NNMI_BUFFER_NOT_URC 	设置接收数据模式为缓存模式，接收到新数据时无 URC 上报
 * 								NNMI_DIRECT_MODE		设置接收数据模式为直吐模式，接收到新数据时通过 URC 立即上报
 * 								NNMI_BUFFER_URC			设置接收数据模式为缓存模式，接收到新数据时仅上报指示 URC
 * @return 	Q_EOK 	成功
 * 			OTHER	失败
 */
int8_t bc25_aep_nnmi(enum_nnmi_status_t nnmi_status)
{
	int8_t result = Q_EOK;
	char send_cmd[50];

	sprintf(send_cmd,AT_NNMI,nnmi_status);
	
	result = at_send_cmd(send_cmd, AT_OK, 0, 3000);
	if(result != Q_EOK)
	{
		LOG_D("check_send_cmd: %s is error,result: %d !",send_cmd, result);
	}
	return result;	
}

/**
 * @description: 发起AEP平台连接
 * @param 	ip		平台地址
 * 			port 	端口
 * @return 	Q_EOK 	成功
 * 			OTHER	失败
 */
int8_t bc25_aep_ncdpopen(char* ip,uint16_t port)
{
	int8_t result = Q_EOK;
	char send_cmd[50];

	sprintf(send_cmd,AT_NCDPOPEN,ip,port);
	
	result = at_send_cmd(send_cmd, AT_OK, 0, 3000);
	if(result != Q_EOK)
	{
		LOG_D("check_send_cmd: %s is error,result: %d !",send_cmd, result);
	}
	return result;	
}

/**
 * @description: 断开连接
 * @param None
 * @return None
 */
int8_t bc25_aep_ncdpclose(void)
{
	int8_t result = Q_EOK;
	
	result = at_send_cmd(AT_NCDPCLOSE, AT_OK, 0, 5000);
	if(result != Q_EOK)
	{
		LOG_D("check_send_cmd: %s is error,result: %d !",AT_NCDPCLOSE, result);
	}	
	return result;
}

/**
 * @description: 发送数据
 * @param None
 * @return 	Q_EOK 	成功
 * 			OTHER	失败
 */
int8_t bc25_aep_nmgs(void)
{
	int8_t result = Q_EOK;
	char send_cmd[200];
	
	signal_report srcStruct;
	AepString aep_str;
	at_qeng_info_t qeng_info;
	
	at_query_qeng(&qeng_info);
	
	srcStruct.cell_id = atoi(qeng_info.sc_cellid);
	srcStruct.ecl = qeng_info.sc_ecl;
	srcStruct.pci = qeng_info.sc_pci;
	srcStruct.rsrp = qeng_info.sc_rsrp;
	srcStruct.sinr = qeng_info.sc_snr;
	
	aep_str = signal_report_CodeDataReport(srcStruct);
	
	sprintf(send_cmd,AT_NMGS_CON,(aep_str.len/2),aep_str.str,NMGS_TYPE_100);
	
	result = at_send_cmd(send_cmd, AT_OK, 0, 3000);
	if(result != Q_EOK)
	{
		LOG_D("check_send_cmd: %s is error,result: %d !",send_cmd, result);
	}
	
	free(aep_str.str);
	
	return result;	
}

