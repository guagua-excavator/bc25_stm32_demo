#include <ctype.h>
#include "AepServiceCodes.h"
#include "dbg_log.h"

#include "middleware.h"


//数据上报:业务数据上报
AepString data_report_CodeDataReport (data_report srcStruct)
{
	char* index;
	AepString resultStruct;
	unsigned short tempLen;

	unsigned short payloadLen = 9;
	resultStruct.len = (1 + 2 + 2 + payloadLen) * 2;
	resultStruct.str = (char *)malloc(resultStruct.len + 1);
	memset(resultStruct.str, 0, resultStruct.len + 1);

	srcStruct.temperature = aep_htoni(srcStruct.temperature);

	index = resultStruct.str;

	memcpy(index, "02", 2);
	index += 1 * 2;

	tempLen = aep_htons(1);//服务ID
	hex_to_str(index, (char *)&tempLen, 2);
	index += 2 * 2;

	tempLen = aep_htons(payloadLen);
	hex_to_str(index, (char *)&tempLen, 2);
	index += 2 * 2;

	hex_to_str(index, (char *)&srcStruct.temperature, 4);
	index += 4 * 2;

	hex_to_str(index, (char *)&srcStruct.fan_left_right, 1);
	index += 1 * 2;

	hex_to_str(index, (char *)&srcStruct.fan_up_down, 1);
	index += 1 * 2;

	hex_to_str(index, (char *)&srcStruct.onoff_state, 1);
	index += 1 * 2;

	hex_to_str(index, (char *)&srcStruct.mode, 1);
	index += 1 * 2;

	hex_to_str(index, (char *)&srcStruct.current_speed, 1);
	index += 1 * 2;


	return resultStruct;
}

//事件上报:故障上报
AepString error_code_report_CodeEventReport (error_code_report srcStruct)
{
	char* index;
	AepString resultStruct;
	unsigned short tempLen;

	unsigned short payloadLen = 1;
	resultStruct.len = (1 + 2 + 2 + payloadLen) * 2;
	resultStruct.str = (char *)malloc(resultStruct.len + 1);
	memset(resultStruct.str, 0, resultStruct.len + 1);


	index = resultStruct.str;

	memcpy(index, "07", 2);
	index += 1 * 2;

	tempLen = aep_htons(1001);//服务ID
	hex_to_str(index, (char *)&tempLen, 2);
	index += 2 * 2;

	tempLen = aep_htons(payloadLen);
	hex_to_str(index, (char *)&tempLen, 2);
	index += 2 * 2;

	hex_to_str(index, (char *)&srcStruct.error_code, 1);
	index += 1 * 2;

	return resultStruct;
}

//指令下发:设定运行状态
int set_run_DecodeCmdDown (char* source, set_run* dest)
{
	char* index = source;
	int srcStrLen = strlen(source);
	int len = 9;

	memset(dest, 0, sizeof(set_run));

	str_to_hex((char *)&dest->onoff_state, index, 1);
	index += 1 * 2;

	str_to_hex((char *)&dest->mode, index, 1);
	index += 1 * 2;

	str_to_hex((char *)&dest->current_speed, index, 1);
	index += 1 * 2;

	str_to_hex((char *)&dest->fan_up_down, index, 1);
	index += 1 * 2;

	str_to_hex((char *)&dest->fan_left_right, index, 1);
	index += 1 * 2;

	str_to_hex((char *)&dest->temperature, index, 4);
	dest->temperature = aep_htoni(dest->temperature);
	index += 4 * 2;

	if (len * 2 > srcStrLen)
	{
		return AEP_CMD_PAYLOAD_PARSING_FAILED;
	}
	return AEP_CMD_SUCCESS;
}

//指令下发响应:设定运行状态响应
AepString set_run_resp_CodeCmdResponse (set_run_resp srcStruct)
{
	char* index;
	AepString resultStruct;
	unsigned short tempLen;

	unsigned short payloadLen = 4;
	resultStruct.len = (1 + 2 + 2 + 2 + payloadLen) * 2;
	resultStruct.str = (char *)malloc(resultStruct.len + 1);
	memset(resultStruct.str, 0, resultStruct.len + 1);

	srcStruct.result = aep_htoni(srcStruct.result);

	index = resultStruct.str;

	memcpy(index, "86", 2);
	index += 1 * 2;

	tempLen = aep_htons(9001);//服务ID
	hex_to_str(index, (char *)&tempLen, 2);
	index += 2 * 2;

	tempLen = aep_htons(srcStruct.taskId);//taskID
	hex_to_str(index, (char *)&tempLen, 2);
	index += 2 * 2;

	tempLen = aep_htons(payloadLen);
	hex_to_str(index, (char *)&tempLen, 2);
	index += 2 * 2;

	hex_to_str(index, (char *)&srcStruct.result, 4);
	index += 4 * 2;


	return resultStruct;
}

//数据上报:信号数据上报
AepString signal_report_CodeDataReport (signal_report srcStruct)
{
	char* index;
	AepString resultStruct;
	unsigned short tempLen;
	unsigned short payloadLen = 20;
	
	resultStruct.len = (1 + 2 + 2 + payloadLen) * 2;
	resultStruct.str = (char *)malloc(resultStruct.len + 1);

	memset(resultStruct.str, 0, resultStruct.len + 1);

	srcStruct.rsrp = aep_htoni(srcStruct.rsrp);
	srcStruct.sinr = aep_htoni(srcStruct.sinr);
	srcStruct.pci = aep_htoni(srcStruct.pci);
	srcStruct.ecl = aep_htoni(srcStruct.ecl);
	srcStruct.cell_id = aep_htoni(srcStruct.cell_id);

		
	index = resultStruct.str;

	memcpy(index, "02", 2);
	index += 1 * 2;

	tempLen = aep_htons(2);//服务ID
	hex_to_str(index, (char *)&tempLen, 2);
	index += 2 * 2;

	tempLen = aep_htons(payloadLen);
	hex_to_str(index, (char *)&tempLen, 2);
	index += 2 * 2;

	hex_to_str(index, (char *)&srcStruct.rsrp, 4);
	index += 4 * 2;

	hex_to_str(index, (char *)&srcStruct.sinr, 4);
	index += 4 * 2;

	hex_to_str(index, (char *)&srcStruct.pci, 4);
	index += 4 * 2;

	hex_to_str(index, (char *)&srcStruct.ecl, 4);
	index += 4 * 2;

	hex_to_str(index, (char *)&srcStruct.cell_id, 4);
	index += 4 * 2;
	

	return resultStruct;
}

AepCmdData decodeCmdDownFromStr(char* source)
{
	char* index;
	AepCmdData result;
	char cmdType;
	unsigned short serviceId;
	unsigned short payloadLen;

	memset(&result, 0, sizeof(AepCmdData));

	index = source;

	// 解析指令类型
	str_to_hex(&cmdType, index, 1);
	index += 1 * 2;
	if (cmdType != 0x06)
	{
		result.code = AEP_CMD_INVALID_DATASET_TYPE;
	}

	//服务Id解析
	str_to_hex((char *)&serviceId, index, 2);
	serviceId = aep_htons(serviceId);
	index += 2 * 2;

	str_to_hex((char *)&result.taskId, index, 2);
	result.taskId = aep_htons(result.taskId);
	index += 2 * 2;

	//payload长度解析
	str_to_hex((char *)&payloadLen, index, 2);
	payloadLen = aep_htons(payloadLen);
	index += 2 * 2;

	if (strlen(index) < payloadLen * 2)
	{
		result.code = AEP_CMD_PAYLOAD_PARSING_FAILED;
		return result;
	}

	if (serviceId == 8001)
	{
		result.serviceIdentifier = "set_run";
		result.data = malloc(sizeof(set_run));
		memset(result.data, 0, sizeof(set_run));
		result.code = set_run_DecodeCmdDown(index, (set_run*)result.data);
	}
	else 
	{
		result.serviceIdentifier = NULL;
		result.data = malloc(payloadLen);
		memset(result.data, 0, sizeof(payloadLen));
		str_to_hex((char *)result.data, index, payloadLen);
		result.code = AEP_CMD_INVALID_DATASET_IDENTIFIER;
	}

	return result;
}

AepCmdData decodeCmdDownFromBytes(char* source, int len)
{
	char * str = malloc(len * 2 + 1);
	AepCmdData result;
	hex_to_str(str, source, len);
	str[len * 2] = 0;
	
	result = decodeCmdDownFromStr(str);
	free(str);
	return result;
}

AepString codeDataReportByIdToStr (int serviceId, void * srcStruct)
{
	if (serviceId == 1)
	{
		return data_report_CodeDataReport(*(data_report*)srcStruct);
	}
	else if (serviceId == 1001)
	{
		return error_code_report_CodeEventReport(*(error_code_report*)srcStruct);
	}
	else if (serviceId == 9001)
	{
		return set_run_resp_CodeCmdResponse(*(set_run_resp*)srcStruct);
	}
	else if (serviceId == 2)
	{
		return signal_report_CodeDataReport(*(signal_report*)srcStruct);
	}
	else 
	{
		AepString result = {0};
		return result;
	}
}

AepBytes codeDataReportByIdToBytes(int serviceId, void * srcStruct)
{
	AepString temp = codeDataReportByIdToStr(serviceId, srcStruct);
	AepBytes result = {0};
	result.len = temp.len / 2;
	if (result.len > 0)
	{
		result.str = malloc(result.len);
		str_to_hex(result.str, temp.str, result.len);
		free(temp.str);
	}
	return result;
}

AepString codeDataReportByIdentifierToStr (char* serviceIdentifier, void * srcStruct)
{
	if (strcmp(serviceIdentifier, "data_report") == 0)
	{
		return data_report_CodeDataReport(*(data_report*)srcStruct);
	}
	else if (strcmp(serviceIdentifier, "error_code_report") == 0)
	{
		return error_code_report_CodeEventReport(*(error_code_report*)srcStruct);
	}
	else if (strcmp(serviceIdentifier, "set_run_resp") == 0)
	{
		return set_run_resp_CodeCmdResponse(*(set_run_resp*)srcStruct);
	}
	else if (strcmp(serviceIdentifier, "signal_report") == 0)
	{
		return signal_report_CodeDataReport(*(signal_report*)srcStruct);
	}
	else 
	{
		AepString result = {0};
		return result;
	}
}

AepBytes codeDataReportByIdentifierToBytes(char* serviceIdentifier, void * srcStruct)
{
	AepString temp = codeDataReportByIdentifierToStr(serviceIdentifier, srcStruct);
	AepBytes result = {0};
	result.len = temp.len / 2;
	if (result.len > 0)
	{
		result.str = malloc(result.len);
		str_to_hex(result.str, temp.str, result.len);
		free(temp.str);
	}
	return result;
}

