/****************************************************************************
*
* Copy right: 2020-, Copyrigths of Quectel Ltd.
****************************************************************************
* File name: dbg_log.c
* History: Rev1.0 2020-12-1
****************************************************************************/

#include <string.h>
#include <stdarg.h>
#include <stdio.h>

#include "usart.h"

#include "dbg_log.h"

/* 指定printf 输出串口 */

#ifdef __GNUC__
/* With GCC, small printf (option LD Linker->Libraries->Small printf set to 'Yes') calls __io_putchar() */
#define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
#define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#endif /* __GNUC__ */

PUTCHAR_PROTOTYPE
{
    /* Place your implementation of fputc here */
    /* e.g. write a character to the USART2 and Loop until the end of transmission */
    HAL_UART_Transmit(&huart1, (uint8_t *)&ch, 1, 0xFFFF);

    return ch;
}

/* log 日志缓存区 */
static char DBG_BUFFER[DBG_BUFF_LEN]= {0};


void dbg_assert_failed(uint8_t* file, uint32_t line)
{
	printf("[ %s():%04d]\n",file, line);
}

void dbg_print(const char *msg,
               const char *func,
               const int   line,
               const char *fmt,...)
{
    va_list arglst;

    va_start(arglst,fmt);
    vsnprintf(DBG_BUFFER,sizeof(DBG_BUFFER),fmt,arglst);
    printf("[ %s ][ %s():%04d] %s\n",msg, func, line, DBG_BUFFER);
    va_end(arglst);
}

void dbg_print_hex_buf(const char *msg, ...)
{
    uint16_t i;
    uint8_t* buf;
    uint32_t cnt;
    va_list arglst;

    va_start(arglst, msg);

    buf = va_arg(arglst, uint8_t*);
    cnt = strlen((char *)buf);

    printf("[%s][%s][L:%d] ", msg,buf,cnt);

    for(i = 0; i < cnt; i++)
    {
        printf("0x%02X ",buf[i]);
    }
    printf("\n");
    va_end(arglst);
}

void dbg_print_buf(const char *msg, ...)
{
    uint32_t cnt;
    va_list arglst;
    uint8_t* buf;
    va_start(arglst, msg);

    buf = va_arg(arglst, uint8_t*);
    cnt = strlen((char *)buf);

    printf("[%s][%s][L:%d] ", msg,buf,cnt);
    printf("\n");

    va_end(arglst);
}
