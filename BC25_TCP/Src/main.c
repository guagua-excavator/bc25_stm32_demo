/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "rtc.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "dbg_log.h"				// debug log
#include "at.h"						// at command
#include "ringbuffer.h"

#include "at_cmd.h"
#include "bc25_tcp.h"

#include "string.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
#define UAER_RECE_PRINT			(0)


typedef enum {

    STATE_MODULE_RESET,
    STATE_MODULE_INIT,
    STATE_SIM_CPIN_STATE,
    STATE_NW_QUERY_STATE,
	STATE_SOC_CFG,
	STATE_SOC_OPEN,
	STATE_SOC_SEND,
	STATE_SOC_SENDEX,
	STATE_SOC_READ,
	STATE_SOC_CLOSE,
    STATE_IDLE,
    STATE_EXCEPTION_STATUS

} enum_tcp_state_t;

// 业务状态机
static enum_tcp_state_t g_tcp_state = STATE_MODULE_RESET;

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
uint8_t g_uart_buf[1];
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */
static void exception_event_callback(void);
static void work_state(void);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
uint32_t start_timeout_tick = 0;
uint8_t  send_data_times = 0;
uint8_t  allow_send_data_flag = 0;
static uint8_t qiopen_timeout_flag = 0;
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_TIM7_Init();
  MX_USART1_UART_Init();
  MX_USART2_UART_Init();
  MX_TIM1_Init();
  MX_RTC_Init();
  /* USER CODE BEGIN 2 */

    bc25_init();

    HAL_TIM_Base_Start_IT(&htim1);
    HAL_UART_Receive_IT(&huart2,g_uart_buf,1);

    LOG_I("AT_Component runing...");

    if (__HAL_PWR_GET_FLAG(PWR_FLAG_SB) != RESET)
    {
        __HAL_PWR_CLEAR_FLAG(PWR_FLAG_SB);
        // 待机模式重启 
        LOG_D("\n The system is standby mode restart!\n");
    }
    else
    {
        // 系统上电重启
        LOG_D("\n The system is powered on!\n");
    }

    /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */

    while (1)
    {
    /* USER CODE END WHILE */

        /* USER CODE BEGIN 3 */
        work_state();                   // 业务逻辑处理状态机
		/* 需用户调用,该函数用于AT命令、URC解析 */
        at_parser();                    // 用于AT-URC解析
        exception_event_callback();     // 事情处理状态机
    }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_LSI|RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_RTC;
  PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSI;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

// 模拟数据
uint8_t send_data[512] = "start0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789end";

/**
 * @description: 业务状态机
 * @param None
 * @return None
 */
static void work_state(void)
{
    // 调试代码：跟踪当前业务运行状态   预编译扩展起来
    static enum_tcp_state_t last_tcp_state = STATE_IDLE;
    if(last_tcp_state != g_tcp_state)
    {
        LOG_I("g_oc_state: <%d>",g_tcp_state);
    }

    switch(g_tcp_state)
    {
    case STATE_MODULE_RESET:

        bc25_reset();
        g_tcp_state = STATE_MODULE_INIT;
        LOG_I("module restart...");
        break;
    case STATE_MODULE_INIT:

        if(bc25_connect() != Q_EOK || at_turn_off_echo() != Q_EOK)
        {
            // 连接失败,重启模组
            g_tcp_state = STATE_MODULE_RESET;
			break;
        }
		else
		{
			LOG_D("bc25 connect successfully!");
		}

        if(at_set_qsclk(QSCLK_DISABLE) == Q_EOK)
        {
            g_tcp_state = STATE_SIM_CPIN_STATE;
        }
        // at_ext_cpsms(CPSMS_DISABLE);
        // at_exe_cedrxs(CEDRXS_DISABLE);
        break;

    case STATE_SIM_CPIN_STATE:

        if(at_query_cpin() != Q_EOK)
        {
            // 多次查CPIN失败,进入低功耗
            set_except_event(EVENT_FINDSIMCARD_ERR);
        }
        else
        {
            g_tcp_state = STATE_NW_QUERY_STATE;
        }
        break;
    case STATE_NW_QUERY_STATE:

        if(at_query_cgpaddr() != Q_EOK)
        {
            /* 清频点动作 */
            set_except_event(NET_EVENT_NET_ERR);
            LOG_I("  multiple requests for access failed!");
        }
        else
        {
            g_tcp_state = STATE_SOC_CFG;
        }
        break;

    case STATE_SOC_CFG:
		
	    if(bc25_tcp_qicfg() == Q_EOK)
        {
            g_tcp_state = STATE_SOC_OPEN;
        }
		else
		{
			HAL_Delay(1000);
		}
        break;
    case STATE_SOC_OPEN:

        if(qiopen_timeout_flag == 0)
        {
            if(bc25_tcp_qiopen() == Q_EOK)
            {
                qiopen_timeout_flag = 1;
                start_timeout_tick = HAL_GetTick();
                LOG_I("send QIOPEN successfully\r\n");
                LOG_I("awaiting QIOPEN urc...\r\n");
            }
            else
            {
                LOG_I("send QIOPEN fail\r\n");
            }
        }
        else		/* 等待URC超时 */
        {
            if((HAL_GetTick() - start_timeout_tick) > 90000)
            {
                set_except_event(QIOPEN_NOURC_EVENT_ERR);

                /* URC超时 进入异常处理机制 计数直接重启 */
                qiopen_timeout_flag = 0;
                LOG_I("open aep URC not reported!");
            }
        }		
        break;
    case STATE_SOC_SEND:
		
		if(bc25_tcp_send(send_data,strlen((char* )send_data)) == Q_EOK)
		{
			g_tcp_state = STATE_IDLE;

			send_data_times = 0;
			allow_send_data_flag = 0;
			
			LOG_I(" send data successfully!");
		}
		else
		{
			LOG_I("QISEND AT SEND fail!");
			set_except_event(QISEND_SEND_DATA_EVENT_FAIL);
		}
        break;
    case STATE_SOC_SENDEX:

        break;
    case STATE_SOC_READ:
		
        break;
    case STATE_SOC_CLOSE:
		
        if(bc25_tcp_qiclose() == Q_EOK)
        {
            g_tcp_state = STATE_SOC_OPEN;
            LOG_I(" tcp connect close success!");
        }
        else
        {
            LOG_I(" tcp connect close fail!");
        }
        break;

    case STATE_IDLE:
		
        if(allow_send_data_flag == 1)
        {
            g_tcp_state = STATE_SOC_SEND;
        }
        break;
		
    case STATE_EXCEPTION_STATUS:
	default:
		break;
	
    }
	
    last_tcp_state = g_tcp_state;
}

/*
*	单位: S 最大时间为(24h-1)s
*	1、MCU与模组进入低功耗(当前MCU进入待机模式)
*	2、当前未使用备份寄存器,系统重启相当于reset
*/
void mcu_low_power_mode (uint32_t timeout)
{
	uint32_t tmp_time = 0;
    RTC_AlarmTypeDef aTime;
    RTC_TimeTypeDef nTime;

    LOG_I("enter deepsleep <1> ");
    // V—BAT 断电
    bc25_power(POWER_DISABLE_MODE);

	HAL_RTC_GetTime(&hrtc,&nTime,RTC_FORMAT_BIN);
	LOG_I("Hours    <%d>",nTime.Hours);
	LOG_I("Minutes  <%d>",nTime.Minutes);
	LOG_I("Seconds  <%d>",nTime.Seconds);	
	
	tmp_time = (nTime.Hours*60*60)+(nTime.Minutes*60)+nTime.Seconds;
	tmp_time = tmp_time + timeout;
	
	// 配置RTC闹钟时间
    aTime.Alarm = 0;
    aTime.AlarmTime.Hours = tmp_time/3600;
    aTime.AlarmTime.Minutes = (tmp_time%3600)/60;
    aTime.AlarmTime.Seconds = tmp_time%3600%60;
    
    HAL_RTC_SetAlarm_IT(&hrtc,&aTime,RTC_FORMAT_BCD);

	LOG_I("Hours    <%d>",aTime.AlarmTime.Hours);
	LOG_I("Minutes  <%d>",aTime.AlarmTime.Minutes);
	LOG_I("Seconds  <%d>",aTime.AlarmTime.Seconds);	

    // 清除标志,进入待机模式
    HAL_PWR_DisableWakeUpPin(PWR_WAKEUP_PIN1);
    __HAL_PWR_CLEAR_FLAG(PWR_FLAG_WU);
    HAL_PWR_EnterSTANDBYMode();

    LOG_I("enter deepsleep <2>");

    while(1)
	{
		HAL_Delay(300);
		HAL_GPIO_TogglePin(LED0_GPIO_Port, LED0_Pin);
		HAL_GPIO_TogglePin(LED1_GPIO_Port, LED1_Pin);
	}
}

/**
 * @description: 事件处理状态机
 * @param None
 * @return None
 */
static void exception_event_callback(void)
{
    static uint8_t exe_err_times = 0;
    static event_except_info_t last_except_info;

    /* 检查是否有事情出现 */
    if(g_except_info == EVENT_END)
    {
        return;
    }

	/* 检查此次事件是否和上次事件相同 */
    if(last_except_info != g_except_info)
    {
        exe_err_times = 0;
    }

    /* 业务状态机配置为事件处理模式 */
    g_tcp_state = STATE_EXCEPTION_STATUS;

    switch((unsigned char)g_except_info)
    {
    case EVENT_FINDSIMCARD_ERR:

        /* 建议MCU进入低功耗,此时查CPIN失败 */
        g_tcp_state = STATE_MODULE_RESET;
		
        break;
    case NET_EVENT_NET_ERR:

        // 需知清频点后 有入网最大建议时间
        if(exe_err_times++ >= 3)
        {
            exe_err_times = 0;
            // 多次找网失败,进入休眠
            LOG_I(" Access failure, Module ready for deepsleep!");

            // mcu和模组进入休眠 单位: S
            mcu_low_power_mode(100);
            break;
        }
        else
        {
            if(at_set_clean_frequency() == Q_EOK)
            {
                g_tcp_state = STATE_NW_QUERY_STATE;
                LOG_I(" frequency clearance successful!");
            }
            else
            {
                g_tcp_state = STATE_MODULE_RESET;
            }
        }
		LOG_I("<-- tcp lccess net < %d > times fail! -->\r\n",exe_err_times);
        break;
		
	case QIOPEN_CONNECT_EVENT_SUCC:

		qiopen_timeout_flag = 0;
        LOG_I("QIOPEN success!");
		g_tcp_state = STATE_SOC_SEND;
		
        break;	
	
	case QIOPEN_CONNECT_EVENT_FAIL:
		
		qiopen_timeout_flag = 0;
        if(exe_err_times++ >= 3)
        {
            exe_err_times = 0;
            // 多次QIOPEN失败,进入休眠
            LOG_I(" qiopen failure, module ready for deepsleep!");
            // mcu和模组进入休眠 单位: S
            mcu_low_power_mode(100);
            break;
        }
        else
        {
			g_tcp_state = STATE_SOC_OPEN;
			HAL_Delay(1000);
        }
		LOG_I("<-- tcp connect < %d > times fail! -->\r\n",exe_err_times);	
		
        break;	
	
	case QIOPEN_NOURC_EVENT_ERR:

        LOG_I(" impossible to happen! ");
        break;
	
	case QICLOSE_EVENT:
		
        LOG_I(" QICLOSE_EVENT! ");
        break;
		
    case QISEND_SEND_DATA_EVENT_SUCC:
		
		break;
    case QISEND_SEND_DATA_EVENT_FAIL:
		
	    if(g_except_info == QISEND_SEND_DATA_EVENT_FAIL)
        {
            // 继续发送,并计数发送错误次数
            g_tcp_state = STATE_SOC_SEND;
            if(exe_err_times++ >= 3)
            {
                exe_err_times = 0;
				g_tcp_state = STATE_SOC_CLOSE;
				break;
            }
            LOG_I("<-- tcp send < %d > times fail! -->\r\n",exe_err_times);
        }
		break;
	case QICLOSE_EVENT_FAIL:
			
	    if(g_except_info == QICLOSE_EVENT_FAIL)
        {
            // 继续关闭
            g_tcp_state = STATE_SOC_CLOSE;
            if(exe_err_times++ >= 3)
            {
				exe_err_times = 0;
				// 多次QICLOSE失败,进入休眠
				LOG_I(" qiclose failure, module ready for deepsleep!");
				// mcu和模组进入休眠 单位: S
				mcu_low_power_mode(100);
				break;
            }
            LOG_I("<-- tcp send < %d > times fail! -->\r\n",exe_err_times);
        }	
		break;		
    default:
        break;
    }
    /* 保存状态机的状态 */
    last_except_info = g_except_info;
    /* 事件状态机置位 */
    g_except_info = EVENT_END;
}

/* 需用户实现,该函数用于将TIM定时器计数清零 */
void tim_clear_count(void)
{
	__HAL_TIM_SET_COUNTER(&htim7,0);
}

/* 需用户实现,该函数用于启动TIM定时器 */
void tim_start_count(void)
{
	HAL_TIM_Base_Start_IT(&htim7);
}

/* 需用户实现,该函数用于向模组直接发送数据 */
void at_send_module(uint8_t* send_buf, uint16_t buf_len)
{
    HAL_UART_Transmit(&huart2,send_buf, buf_len,0xffffffff);
}

/**
 * @description: 定时器中断服务函数   htim7: 单位: 10MS   htim1: 单位: 1S
 * @param {type} 
 * @return {type} 
 */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
    // 用于数据AT USART数据接收完成标记使用,小心操作
    if(htim == (&htim7))
    {
		/* 需用户调用,该函数用于标记TIM计数时间结束标记状态 */
        set_uart_recv_sta();
        HAL_TIM_Base_Stop_IT(&htim7);
    }
    else if(htim == (&htim1))
    {
        HAL_GPIO_TogglePin(LED1_GPIO_Port, LED1_Pin);
        if(send_data_times++ > 5)
        {
           allow_send_data_flag = 1;
        }
    }
}

/**
 * @description: 串口中断服务函数
 * @param huart 串口号
 * @return None
 */
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
    if(huart->Instance == USART2)
    {
#if UAER_RECE_PRINT
		HAL_UART_Transmit(&huart1,g_uart_buf,1,1000);		// 接收到数据马上使用串口1发送出去
		while(__HAL_UART_GET_FLAG(&huart1,UART_FLAG_TC)!=SET);
#endif
        HAL_UART_Receive_IT(&huart2,g_uart_buf,1);			// 重新使能串口2接收中断
		
		/* 需用户调用,该函数用于存储数据接收 */
		rb_data_putchar(g_uart_buf[0]);
    }
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
    /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
    /* User can add his own implementation to report the file name and line number,
       tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
